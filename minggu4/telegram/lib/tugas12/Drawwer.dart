import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'models/Chart_model.dart';

class DrawerScreen extends StatefulWidget{
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen>{
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text('M.Rizky Ramadhani'),
            currentAccountPicture: CircleAvatar(
            backgroundImage: AssetImage('assets/images/zain.jpg'),
          ),
          accountEmail: Text("rama72720@gmail.com"),
          ),
          DrawerListTile(
            iconData: Icons.group,
            tittle: 'NewGroup',
            onTilePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.lock,
            tittle: 'New Channel Chat',
            onTilePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.contacts,
            tittle: 'contacts',
            onTilePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            tittle: 'Saved Message',
            onTilePressed: (){},
          ),
          DrawerListTile(
            iconData: Icons.phone,
            tittle: 'Calls',
            onTilePressed: (){},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget{
  final IconData? iconData;
  final String? tittle;
  final VoidCallback? onTilePressed;

  const DrawerListTile({Key? key, this.iconData, this.tittle, this.onTilePressed}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading : Icon(iconData),
      title: Text(tittle!, style: TextStyle(fontSize: 16),),
    );
  }
}