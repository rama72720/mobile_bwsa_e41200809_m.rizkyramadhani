import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'LoginScreen.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options:FirebaseOptions(
    apiKey : 'AIzaSyCL6e5i_xz4sZ_nxOH_8FpWhXJyHwy-nBc',
    appId : '1:344458961056:android:36404e1e293f664f7aa9c5',
    messagingSenderId: '344458961056',
    projectId: 'authenticaton-619da',
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
       home: LoginScreen(),
    );
  }
}