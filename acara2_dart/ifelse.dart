import 'dart:io';

void main(List<String> args) {
  print('Kita akan memasuki di dunia game Werewolf');
  stdout.write('Masukkan nama anda : ');
  String nama = stdin.readLineSync()!;

  if (nama == '') {
    print('Nama harus di isi!');
  } else {
    var peran1 = 'Penyihir', peran2 = 'Guard', peran3 = 'Werewolf';

    stdout.write('Pilih peran yang anda inginkan : ');
    String peranPlayer = stdin.readLineSync()!;

    if (peranPlayer == '') {
      print('$nama Pilih peranmu untuk memulai game');
    } else if (peranPlayer == peran1) {
      print(
          'Hello $peran1 $nama kamu dapat melihat siapa yang menjadi Werewolf');
    } else if (peranPlayer == peran2) {
      print(
          'Hello $peran2 $nama kamu akan membantu melindungi temanmu dari serangan Werewolf');
    } else if (peranPlayer == peran3) {
      print('Hello $peran3 $nama kamu akan memakan mangsa setiap malam');
    } else {
      print('Pilih peranmu untuk memulai game');
    }
  }
}


