import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main() {
  Bangun_datar bangun_datar = new Bangun_datar();
  Lingkaran lingkaran = new Lingkaran(12, 3.14);
  Persegi persegi = new Persegi(12);
  Segitiga segitiga = new Segitiga(3, 4, 5);

  bangun_datar.luas();
  bangun_datar.keliling();

  print("Luas lingkaran : ${lingkaran.luas()}");
  print("Keliling lingkaran : ${lingkaran.keliling()}");
  print("Luas persegi : ${persegi.luas()}");
  print("Keliling persegi : ${persegi.keliling()}");
  print("Luas segitiga : ${segitiga.luas()}");
  print("Keliling segitiga : ${segitiga.keliling()}");
}
