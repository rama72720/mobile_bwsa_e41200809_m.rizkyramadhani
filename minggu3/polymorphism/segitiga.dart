import 'bangun_datar.dart';

class Segitiga extends Bangun_datar {
  double? alas;
  double? tinggi;
  double? sisiMiring;

  Segitiga(double alas, double tinggi, double sisiMiring) {
    this.alas = alas;
    this.tinggi = tinggi;
    this.sisiMiring = sisiMiring;
  }

  @override
  double luas() {
    return (alas! * tinggi!) / 2;
  }

  @override
  double keliling(){
    return alas! + tinggi! + sisiMiring!;
  }
}
